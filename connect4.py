# actually the connect 4 model/controller
from colorama import init,Fore, Back, Style
from pyfiglet import Figlet
import termcolor
import sys
init()

class cBoard(object):

    def __init__(self, dx=7, dy=6):
        self.board = [[0 for y in range(dy)] for x in range(dx)]
        self.dx = self.cx = dx  # current size values
        self.dy = self.cy = dy
        self.turn = 1
        self.searchstart = False
        self.last = (False, False)
        self.score = [0, 0]

    def getScore(self, i):
        return self.score[i-1]

    def get(self, i, j):
        return self.board[j][i]

    def place(self, column):
        for h in range(self.cy):
            if self.get(column, h) == 0:
                self.board[h][column] = self.turn
                self.last = (column, h)
                if self.turn == 1:
                    self.turn = 2
                else:
                    self.turn = 1
                break

    # returns list of available columns
    def available(self):
        result = []
        for i in range(self.cx):
            j = 0
            col = False
            while not col and j < self.cy:
                col = col or not self.get(i, j)
                j += 1
            if col:
                result.append(i)
        return result

    def valid(self, i, j):
        return i < self.cy and j < self.cx and i >= 0 and j >= 0

    def match(self, i, j):
        return self.get(i,j) == self.searchstart

    def search(self, i, j, oi, oj):

        #if position is vaild
        if self.valid(i+oi, j + oj):
            i = i + oi
            j = j + oj # update location

            # if position is a match, continue search
            if j < self.cy and i < self.cx and self.match(i, j):
                return 1 + self.search(i, j, oi, oj)
            else:
                return 0
        else:
            return 0 # if not valid

    def checkwin(self):

        result = self.checkwin_loc(self.last[0],self.last[1])

        player = self.get(self.last[0], self.last[1]) - 1

        self.score[player] += result + 1

        if result == 4:
            self.score[player] += 10

        return result == 4

    def checkwin_loc(self, i, j):
        # set of possible unique offsets

        self.searchstart = self.get(i,j)

        maxstreak = 0

        offsets = [(1,0),(0,1),(1,1),(-1,1)]

        # for each direction in the offsets
        for dir in offsets:
            streak = 1 # reset streak

            # search direction
            streak += self.search(i,j,dir[0],dir[1])


            # if not completed, search opposite direction
            if streak < 4:
                streak += self.search(i,j,dir[0]*-1,dir[1]*-1)

            if streak > maxstreak:
                maxstreak = streak

            # if completed, return true and playerid
            if streak == 4:
                return maxstreak

        # if all directions searched
        return maxstreak

    def reset(self):
        self.board = [[0 for x in range(self.dx)] for y in range(self.dy)]
        self.score = [0, 0]
        self.turn = 1
        return

class cController(object):
    def __init__(self):
        self.won = False

    def play(self):

        f = Figlet(font='slant')
        print(Fore.LIGHTRED_EX)
        print(f.renderText('Connect'))
        f = Figlet(font='epic')
        print(Fore.LIGHTYELLOW_EX)
        print(f.renderText(' four'))

        print(Fore.LIGHTWHITE_EX)
        width = int(input("What width board would you like to play on? (7 is standard)"))
        height = int(input("What height board would you like to play on? (6 is standard)"))

        self.myboard = cBoard(width, height)
        self.myboard.reset()
        while not self.won:
            turn = self.myboard.turn

            color = Fore.LIGHTCYAN_EX
            if (self.myboard.turn == 1):
                color = Fore.LIGHTRED_EX

            print(Fore.WHITE)
            print("It's" + color + " player " + str(self.myboard.turn) + "'s " + Fore.WHITE + "turn")
            print(color + "Player " + str(turn) + Fore.WHITE + " has " + str(self.myboard.getScore(turn)) + " points!")
            self.output()

            if (len(self.myboard.available()) > 0):
                print("Available Slots: " + str(self.myboard.available()))
                slot = int(input("Select a slot: "))

                while slot not in self.myboard.available():
                    print("Available Slots: " + str(self.myboard.available()))
                    slot = int(input("Select an AVAILABLE slot: "))

                self.myboard.place(slot)
                self.won = self.myboard.checkwin()

            else:
                print("Stalemate detected.")
                if (self.myboard.getScore(1) > self.myboard.getScore(2)):
                    print(Fore.LIGHTRED_EX + "Player 1 wins with" + str(self.myboard.getScore(1)) + " points!")
                elif (self.myboard.getScore(1) < self.myboard.getScore(2)):
                    print(Fore.LIGHTCYAN_EX + "Player 2 wins with" + str(self.myboard.getScore(2)) + " points!")
                else:
                    print("It's a tie.")
                self.won = True

        if self.won:
            self.output()
            print(color + "Player " + str(turn) + Fore.WHITE + " won!")
            print(Fore.LIGHTRED_EX + "Player " + str(1) + Fore.WHITE + " had " + str(self.myboard.getScore(1)) + " points!")
            print(Fore.LIGHTCYAN_EX + "Player " + str(2) + Fore.WHITE + " had " + str(self.myboard.getScore(2)) + " points!")
            mvp = input("Want to play again? y/n ") == "y"
            if mvp:
                self.won = False
                self.play()


        return 0

    def output(self):
        maxy = self.myboard.cy
        for y in range(0,self.myboard.cy):
            for x in range(0, self.myboard.cx):
                ccolor = Fore.WHITE
                if (self.myboard.board[maxy-y-1][x] == 1):
                    ccolor = Fore.LIGHTRED_EX
                elif(self.myboard.board[maxy-y-1][x] == 2):
                    ccolor = Fore.LIGHTCYAN_EX
                sys.stdout.write(ccolor + str(self.myboard.board[maxy-y-1][x]) + ' ')
                sys.stdout.write(Fore.WHITE)
            sys.stdout.write('\n')
