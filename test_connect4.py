# unit tests for connect4 controller/model

import unittest

import connect4 as c4


class TestModel(unittest.TestCase):
    def testConstructor(self):
        self.x = c4.cBoard()

        self.xSize = self.x.cx
        self.expectedXSize = 7
        self.ySize = self.x.cy
        self.expectedYSize = 6
        self.curTurn = self.x.turn
        self.expectedTurn = 1
        self.expectedPlacement = 0

        self.assertEqual(self.xSize, self.expectedXSize)
        self.assertEqual(self.ySize, self.expectedYSize)
        self.assertEqual(self.curTurn, self.expectedTurn)

        for i in range(self.x.cx):
            for j in range(self.x.cy):
                self.assertEqual(self.x.board[i][j], self.expectedPlacement)

    def testGet(self):
        self.x = c4.cBoard()

        self.expected = 0
        self.curPlacement = self.x.get(4, 5)
        self.assertEqual(self.expected, self.curPlacement)

    def testPlace(self):
        self.x = c4.cBoard()

        self.expected = 1

        self.x.place(4)
        self.curPlacement = self.x.get(4, 0)

        self.assertEqual(self.expected, self.curPlacement)

    def testAvailable(self):
        self.x = c4.cBoard()
        self.x.reset()

        self.expectedNumColumns = 7
        self.curNumColumns = len(self.x.available())
        self.assertEqual(self.expectedNumColumns, self.curNumColumns)

        for i in range(self.x.cy):
            for y in range(0, self.x.cy):
                self.x.place(0)

        self.expectedNumColumns = 6
        self.curNumColumns = len(self.x.available())
        self.assertEqual(self.expectedNumColumns, self.curNumColumns)

    def testCheckWin(self):
        self.x = c4.cBoard()
        self.x.reset()

        self.expected = True
        self.x.place(0)
        self.x.place(5)
        self.x.place(0)
        self.x.place(5)
        self.x.place(0)
        self.x.place(5)
        self.x.place(0)

        self.actual = self.x.checkwin()

        self.assertEqual(self.expected, self.actual)

    def testReset(self):
        self.x = c4.cBoard()
        self.x.place(0)
        self.x.place(0)
        self.x.place(1)

        self.x.reset()

        # same as constructor test
        self.xSize = self.x.cx
        self.expectedXSize = 7
        self.ySize = self.x.cy
        self.expectedYSize = 6
        self.curTurn = self.x.turn
        self.expectedTurn = 1
        self.expectedPlacement = 0

        self.assertEqual(self.xSize, self.expectedXSize)
        self.assertEqual(self.ySize, self.expectedYSize)
        self.assertEqual(self.curTurn, self.expectedTurn)

        for i in range(self.x.cx):
            for j in range(self.x.cy):
                self.assertEqual(self.x.board[j][i], self.expectedPlacement)

    def testScore(self):
        self.x = c4.cBoard()

        self.x.place(0)
        self.x.checkwin()
        self.expectedScore = 2
        self.curScore = self.x.getScore(1)
        self.assertEqual(self.curScore, self.expectedScore)

        self.x.place(5)
        self.x.checkwin()
        self.expectedScore = 2
        self.curScore = self.x.getScore(2)
        self.assertEqual(self.curScore, self.expectedScore)

        self.x.place(0)
        self.x.checkwin()
        self.expectedScore = 5
        self.curScore = self.x.getScore(1)
        self.assertEqual(self.curScore, self.expectedScore)

        self.x.place(3)
        self.x.checkwin()
        self.expectedScore = 4

        self.curScore = self.x.getScore(2)
        self.assertEqual(self.curScore, self.expectedScore)

        self.x.place(0)
        self.x.checkwin()
        self.expectedScore = 9
        self.curScore = self.x.getScore(1)
        self.assertEqual(self.curScore, self.expectedScore)

        self.x.place(4)
        self.x.checkwin()
        self.expectedScore = 8
        self.curScore = self.x.getScore(2)
        self.assertEqual(self.curScore, self.expectedScore)

        self.x.place(0)
        self.x.checkwin()
        self.expectedScore = 24
        self.curScore = self.x.getScore(1)
        self.assertEqual(self.curScore, self.expectedScore)

    def testResize(self):
        self.x = c4.cBoard(10, 9)
        self.xSize = self.x.cx
        self.expectedXSize = 10
        self.ySize = self.x.cy
        self.expectedYSize = 9
        self.curTurn = self.x.turn
        self.expectedTurn = 1
        self.expectedPlacement = 0

        self.assertEqual(self.xSize, self.expectedXSize)
        self.assertEqual(self.ySize, self.expectedYSize)
        self.assertEqual(self.curTurn, self.expectedTurn)

        for i in range(self.x.cx):
            for j in range(self.x.cy):
                self.assertEqual(self.x.board[i][j], self.expectedPlacement)


if __name__ == '__main__':
    unittest.main()
